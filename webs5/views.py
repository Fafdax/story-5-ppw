from django.shortcuts import render, redirect
from .models import Matkul
from .forms import MatkulForm
from django.contrib import messages

# Create your views here.
def Story1(request):
    return render(request, 'Story1.html')

def Story3_aboutme(request):
    return render(request, 'Story3_aboutme.html')

def Story3_contact(request):
    return render(request, 'Story3_contact.html')

def Story3_eduskill(request):
    return render(request, 'Story3_eduskill.html')

def home(request):
    return render(request, 'home.html')

def Story5_matkul(request):
    context = {
        "lessons" : Matkul.objects.all().order_by("-id"),
    }
    return render(request, "Story5_matkul.html", context)

def Story5_tambahMatkul(request):
    if request.method == "POST":
        mForm = MatkulForm(request.POST)
        if mForm.is_valid():
            mForm.save()
            messages.add_message(request, messages.SUCCESS, "Mata kuliah {} telah berhasil ditambahkan!".format(request.POST["nama_matkul"]))
        return redirect("webs5:s5matkul")
    return render(request, "Story5_tambahMatkul.html")

def Story5_detailMatkul(request, id):
    mataKuliah = Matkul.objects.get(id=id)
    context = {
        "matkul" : mataKuliah,
    }
    return render(request, "Story5_detailMatkul.html", context)

def hapus_matkul(request, id):
    if request.method == "POST":
        mkul = Matkul.objects.get(id=id)
        mkul.delete()
        messages.add_message(request, messages.SUCCESS, "Mata kuliah {} telah berhasil dihapus!".format(mkul.nama_matkul))
    return redirect("webs5:s5matkul")
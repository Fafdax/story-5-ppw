from django.db import models
from django.shortcuts import reverse

# Create your models here.
pilihan_semester = [
    ("2019/2020 Gasal","2019/2020 Gasal"),
    ("2019/2020 Genap","2019/2020 Genap"),
    ("2020/2021 Gasal","2020/2021 Gasal"),
    ("2020/2021 Genap","2020/2021 Genap"),
    ("2021/2022 Gasal","2021/2022 Gasal"),
    ("2021/2022 Genap","2021/2022 Genap"),
    ("2022/2023 Gasal","2022/2023 Gasal"),
    ("2022/2023 Genap","2022/2023 Genap")
]

class Matkul(models.Model):
    nama_matkul = models.TextField(max_length = 50)
    nama_dsn = models.TextField(max_length = 50)
    sks = models.IntegerField()
    deskripsi = models.TextField(max_length = 300)
    semester = models.TextField(choices = pilihan_semester)
    ruangan = models.TextField(max_length = 40)

    def __str__(self):
        return "{} ({})".format(self.nama_matkul, self.nama_dsn)
    
    def get_absolute_url(self):
        return reverse("webs5:s5dm", args=[self.id])
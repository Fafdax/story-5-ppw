from django.urls import path

from . import views
from .views import Story5_matkul, Story5_tambahMatkul, Story5_detailMatkul, hapus_matkul

app_name = 'webs5'

urlpatterns = [
    path('', views.Story1, name='Story1'),
    path('Story3_aboutme', views.Story3_aboutme, name='aboutme'),
    path('Story3_contact', views.Story3_contact, name='contact'),
    path('Story3_eduskill', views.Story3_eduskill, name='eduskill'),
    path('home', views.home, name='home'),
    path('Story5_matkul', views.Story5_matkul, name='s5matkul'),
    path('Story5_tambahMatkul', views.Story5_tambahMatkul, name='s5tm'),
    path('matkul/<int:id>', views.Story5_detailMatkul, name='s5dm'),
    path('hapus_matkul/<int:id>', views.hapus_matkul, name='delete'),
]
